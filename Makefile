TEX_FILES=$(shell find . -type f -regex ".*\.tex")
BIBLIO=Bibliographie.bib
MASTER=master.tex
PDF=$(MASTER:.tex=.pdf)
AUX=$(MASTER:.tex=.aux)

.PHONY: clean all

%.pdf: $(TEX_FILES) $(BIBLIO)
	pdflatex $(MASTER)
	bibtex $(AUX)
	pdflatex $(MASTER)
	bibtex $(AUX)
	pdflatex $(MASTER)

all: $(PDF)

clean:
	@rm -f *.{aux,bbl,blg,lof,log,maf,mtc*,out,toc}
